import React, { useRef } from "react";
import { useDrag } from "react-dnd";
import Component from "./Component";
import { PAGE_COLUMN, PAGE_COMPONENT, SIDEBAR_COMPONENT } from "./constants";
import DropZone from "./DropZone";

const style = {};
const Column = ({ data, components, handleDrop, path, innerRef }) => {
    const ref = useRef(null);
    const [{ isDragging }, drag] = useDrag({
        item: {
            type: PAGE_COLUMN,
            id: data.id,
            children: data.children,
            path
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging()
        })
    });

    const opacity = isDragging ? 0 : 1;
    drag(ref);

    const renderComponent = (component, currentPath) => {
        return (
            <Component
                key={component.id}
                data={component}
                components={components}
                path={currentPath}
            />
        );
    };

    return (
        <div id={`col-${path}`} ref={innerRef} className="columnContainer">
            <div
                ref={ref}
                style={{ ...style, opacity }}
                className="base draggable column"
            >
                {data.id}
                {data.children.map((component, index) => {
                    const currentPath = `${path}-${index}`;

                    return (
                        <React.Fragment key={component.id}>
                            <DropZone
                                data={{
                                    path: currentPath,
                                    childrenCount: data.children.length
                                }}
                                onDrop={handleDrop}
                                ACCEPTS = {[SIDEBAR_COMPONENT, PAGE_COMPONENT]}
                            />
                            {renderComponent(component, currentPath)}
                        </React.Fragment>
                    );
                })}
                <DropZone
                    data={{
                        path: `${path}-${data.children.length}`,
                        childrenCount: data.children.length
                    }}
                    onDrop={handleDrop}
                    ACCEPTS = {[SIDEBAR_COMPONENT, PAGE_COMPONENT]}
                    isLast
                />
            </div>
        </div>
    );
};
export default Column;
