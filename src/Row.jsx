import React, { useRef, useState } from "react";
import { useDrag } from "react-dnd";
import Column from "./Column";
import { PAGE_COLUMN, PAGE_COMPONENT, PAGE_ROW, SIDEBAR_COLUMN, SIDEBAR_COMPONENT } from "./constants";
import DropZone from "./DropZone";

const style = {};
const Row = ({ data, components, handleDrop, path }) => {
    const handler = useRef(null);
    const columnWrapper = useRef(null);
    const columnContainer = useRef(null);
    const ref = useRef(null);
    const [isShowDropzone, setIsShowDropzone] = useState(true);
    const [targetedColumn, setTargetedColumn] = useState(null);

    const [{ isDragging }, drag] = useDrag({
        item: {
            type: PAGE_ROW,
            id: data.id,
            children: data.children,
            path
        },
        canDrag: true,
        collect: (monitor) => ({
            isDragging: monitor.isDragging()
        })
    });
    drag(ref);
    const opacity = isDragging ? 0 : 1;

    const renderColumn = (column, currentPath, ref) => {
        return (
            <Column
                innerRef={ref}
                key={column.id}
                data={column}
                components={components}
                handleDrop={handleDrop}
                path={currentPath}
            />
        );
    };

    return (
        <div
            ref={ref}
            style={{ ...style, opacity }}
            className="base draggable row"
        >
            {data.id}
            {data.children.length == 0 ? (
                <DropZone
                    data={{
                        path: `${path}-0`,
                        childrenCount: data.children.length
                    }}
                    onDrop={handleDrop}
                    ACCEPTS = {[SIDEBAR_COMPONENT, SIDEBAR_COLUMN, PAGE_COMPONENT, PAGE_COLUMN]}
                />
            ) : (
                <div ref={columnWrapper} className="columns">
                    {data.children.map((column, index) => {
                        const currentPath = `${path}-${index}`;

                        return (
                            <React.Fragment key={column.id}>
                                {isShowDropzone || index == 0 ? (
                                    <DropZone
                                        data={{
                                            path: currentPath,
                                            childrenCount: data.children.length
                                        }}
                                        onDrop={handleDrop}
                                        ACCEPTS = {[SIDEBAR_COMPONENT,SIDEBAR_COLUMN, PAGE_COMPONENT, PAGE_COLUMN]}
                                        className="horizontalDrag"
                                    
                                    />
                                ) : (
                                    <div
                                        ref={handler}
                                        id={`handle-${currentPath}`}
                                        className="handler"
                                    />
                                )}
                                {renderColumn(
                                    column,
                                    currentPath,
                                    columnContainer
                                )}
                            </React.Fragment>
                        );
                    })}
                    <DropZone
                        data={{
                            path: `${path}-${data.children.length}`,
                            childrenCount: data.children.length
                        }}
                        onDrop={handleDrop}
                        ACCEPTS = {[SIDEBAR_COMPONENT,SIDEBAR_COLUMN, PAGE_COMPONENT, PAGE_COLUMN]}
                        className="horizontalDrag"
                        isLast
                    />
                </div>
            )}
        </div>
    );
};
export default Row;
