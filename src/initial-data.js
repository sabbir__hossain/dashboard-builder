import { PAGE_COLUMN, PAGE_COMPONENT, PAGE_ROW } from "./constants";

const initialData = {
  layout: [
    {
      type: PAGE_ROW,
      id: "row0",
      children: [
        {
          type: PAGE_COLUMN,
          id: "column0",
          children: [
            {
              type: PAGE_COMPONENT,
              id: "component0",
              componentDetails:{
                name: "Button",
                btnText:'Custom Button',
                attributes:{type:'button'},
                style:{backgroundColor:'red'}
              }
            },
            {
              type: PAGE_COMPONENT,
              id: "component1"
            }
          ]
        },
        {
          type: PAGE_COLUMN,
          id: "column1",
          children: [
            {
              type: PAGE_COMPONENT,
              id: "component2"
            }
          ]
        }
      ]
    },
    {
      type: PAGE_ROW,
      id: "row1",
      children: [
        {
          type: PAGE_COLUMN,
          id: "column2",
          children: [
            {
              type: PAGE_COMPONENT,
              id: "component3"
            },
            {
              type: PAGE_COMPONENT,
              id: "component0"
            },
            {
              type: PAGE_COMPONENT,
              id: "component2"
            }
          ]
        }
      ]
    }
  ],
  components: {
    component0: { id: "component0", type: "input", content: "Some input" },
    component1: { id: "component1", type: "image", content: "Some image" },
    component2: { id: "component2", type: "email", content: "Some email" },
    component3: { id: "component3", type: "name", content: "Some name" },
    component4: { id: "component4", type: "phone", content: "Some phone" }
  }
};

export default initialData;
