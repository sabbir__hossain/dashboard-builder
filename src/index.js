import React from "react";
import { DndProvider } from "react-dnd";
import Backend from "react-dnd-html5-backend";
import ReactDOM from "react-dom/client";
import Example from "./example";

import "./styles.css";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <DndProvider backend={Backend}>
  <Example />
</DndProvider>
);