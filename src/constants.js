import shortid from "shortid";

export const SIDEBAR_COMPONENT = "sidebarItem";
export const SIDEBAR_ROW = "sidebarRow";
export const SIDEBAR_COLUMN = "sidebarColumn";
export const PAGE_ROW = "row";
export const PAGE_COLUMN = "column";
export const PAGE_COMPONENT = "component";


export const SIDEBAR_ITEMS = [
  {
    id: shortid.generate(),
    type: SIDEBAR_COMPONENT,
    component: {
      type: "input",
      content: "Some input"
    }
  },
  {
    id: shortid.generate(),
    type: SIDEBAR_COMPONENT,
    component: {
      type: "name",
      content: "Some name"
    }
  },
  {
    id: shortid.generate(),
    type: SIDEBAR_COMPONENT,
    component: {
      type: "email",
      content: "Some email"
    }
  },
  {
    id: shortid.generate(),
    type: SIDEBAR_COMPONENT,
    component: {
      type: "phone",
      content: "Some phone"
    }
  },
  {
    id: shortid.generate(),
    type: SIDEBAR_COMPONENT,
    component: {
      type: "image",
      content: "Some image"
    }
  },
  {
    id: shortid.generate(),
    type: SIDEBAR_ROW,
    component: {
      type: PAGE_ROW,
      content: "Row Content"
    }
  },
  {
    id: shortid.generate(),
    type: SIDEBAR_COLUMN,
    component: {
      type: PAGE_COLUMN,
      content: "Column Content"
    }
  }
];
